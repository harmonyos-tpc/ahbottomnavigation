package com.aurelhubert.ahbottomnavigation;

import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.Optional;

public class AHBottomNavigationItem {
    private String title = "";
    private int color = Color.GRAY.getValue();

    private PixelMap orgPixelMap;

    private PixelMap activePixelMap;
    private PixelMap inActivePixelMap;
    private PixelMap disablePixelMap;
    private PixelMap coloredInActivePixelMap;
    private PixelMap coloredActivePixelMap;
    private int iconSize;

    private int drawableRes;

    private AHBottomNavigation ahBottomNavigation;

    public AHBottomNavigationItem(String title, int resource, Context context) {
        this.title = title;
        this.drawableRes = resource;
        Optional<PixelMap> optional = ResUtil.getPixelMap(context, drawableRes);
        orgPixelMap = optional.isPresent() ? optional.get() : null;
    }

    public AHBottomNavigationItem(String title, int resource, int color, Context context) {
        this.title = title;
        this.drawableRes = resource;
        this.color = color;
        Optional<PixelMap> optional = ResUtil.getPixelMap(context, drawableRes);
        orgPixelMap = optional.isPresent() ? optional.get() : null;
    }

    public AHBottomNavigationItem(String title, PixelMap pixelMap) {
        this.title = title;
        this.orgPixelMap = pixelMap;
    }

    public AHBottomNavigationItem(String title, PixelMap pixelMap, int color) {
        this.title = title;
        this.orgPixelMap = pixelMap;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    PixelMap getOrgPixelMap() {
        return orgPixelMap;
    }

    PixelMap getActivePixelMap() {
        return activePixelMap;
    }

    PixelMap getInActivePixelMap() {
        return inActivePixelMap;
    }

    PixelMap getDisablePixelMap() {
        return disablePixelMap;
    }

    PixelMap getColoredInActivePixelMap() {
        return coloredInActivePixelMap;
    }

    PixelMap getColoredActivePixelMap() {
        return coloredActivePixelMap;
    }

    void setupPixelMap(AHBottomNavigation ahBottomNavigation, int iconSize) {
        this.ahBottomNavigation = ahBottomNavigation;
        this.iconSize = iconSize;
        if (orgPixelMap != null) {
            generateIconBitmaps(ahBottomNavigation);
        }
    }

    private void generateIconBitmaps(AHBottomNavigation ahBottomNavigation) {
        if (orgPixelMap == null) {
            return;
        }
        orgPixelMap = scaleIcon(orgPixelMap);
        PixelMap origin = orgPixelMap;
        activePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        Canvas canvas = new Canvas(new Texture(activePixelMap));
        canvas.drawColor(ahBottomNavigation.getTitleColorActive(), Canvas.PorterDuffMode.SRC_IN);
        inActivePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        canvas = new Canvas(new Texture(inActivePixelMap));
        canvas.drawColor(ahBottomNavigation.getTitleColorInactive(), Canvas.PorterDuffMode.SRC_IN);
        coloredActivePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        canvas = new Canvas(new Texture(coloredActivePixelMap));
        canvas.drawColor(ahBottomNavigation.getColoredTitleColorActive(), Canvas.PorterDuffMode.SRC_IN);
        coloredInActivePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        canvas = new Canvas(new Texture(coloredInActivePixelMap));
        canvas.drawColor(ahBottomNavigation.getColoredTitleColorInactive(), Canvas.PorterDuffMode.SRC_IN);
        disablePixelMap = copy(origin, origin.getImageInfo().size.width, origin.getImageInfo().size.height);
        canvas = new Canvas(new Texture(disablePixelMap));
        canvas.drawColor(ahBottomNavigation.getItemDisableColor(), Canvas.PorterDuffMode.SRC_IN);
    }

    private PixelMap scaleIcon(PixelMap origin) {
        int width = origin.getImageInfo().size.width;
        int height = origin.getImageInfo().size.height;
        int size = Math.max(width, height);
        if (size == iconSize) {
            return origin;
        } else if (size > iconSize) {
            int scaledWidth;
            int scaledHeight;
            if (width > iconSize) {
                scaledWidth = iconSize;
                scaledHeight = (int) (iconSize * ((float) height / width));
            } else {
                scaledHeight = iconSize;
                scaledWidth = (int) (iconSize * ((float) width / height));
            }
            return copy(origin, scaledWidth, scaledHeight);
        } else {
            return origin;
        }
    }

    private PixelMap copy(PixelMap source, int width, int height) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.alphaType = AlphaType.PREMUL;
        initializationOptions.size = new Size(width, height);
        return PixelMap.create(source, initializationOptions);
    }
}
