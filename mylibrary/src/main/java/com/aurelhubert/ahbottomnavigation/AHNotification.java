package com.aurelhubert.ahbottomnavigation;

import java.util.ArrayList;
import java.util.List;

public class AHNotification {
    private String text;

    private int textColor;

    private int backgroundColor;

    public AHNotification() {}

    public boolean isEmpty() {
        if (text == null || text.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String getText() {
        return text;
    }

    public int getTextColor() {
        return textColor;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public static AHNotification justText(String text) {
        return new Builder().setText(text).build();
    }

    public static List<AHNotification> generateEmptyList(int size) {
        List<AHNotification> notificationList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            notificationList.add(new AHNotification());
        }
        return notificationList;
    }

    public static class Builder {
        private String text;
        private int textColor;
        private int backgroundColor;

        public Builder setText(String text) {
            this.text = text;
            return this;
        }

        public Builder setTextColor(int textColor) {
            this.textColor = textColor;
            return this;
        }

        public Builder setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public AHNotification build() {
            AHNotification notification = new AHNotification();
            notification.text = text;
            notification.textColor = textColor;
            notification.backgroundColor = backgroundColor;
            return notification;
        }
    }
}
