# Demo

<img src="art/ahbottomnavigation_demo.gif" width="40%"/>

# how to user?

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:ahbottomnavigation:1.0.0'
}
```

```xml

<com.aurelhubert.ahbottomnavigation.AHBottomNavigation
        ohos:id="$+id:bn"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:align_parent_bottom="true"/>
```

 ```java
        bottomNavigation = (AHBottomNavigation)findComponentById(ResourceTable.Id_bn);
        bottomNavigation.setColored(false);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Menu 1", ResourceTable.Media_ic_home_white_24dp, 0xff455C65, getContext());
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Menu 2", ResourceTable.Media_ic_maps_local_bar, 0xff00886A, getContext());
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Menu 3", ResourceTable.Media_ic_maps_local_restaurant, 0xff8B6B62, getContext());

        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);
        bottomNavigationItems.add(item3);
        bottomNavigation.setSelectHideNotification(false);
        bottomNavigation.addItems(bottomNavigationItems);
        AHNotification notification = new AHNotification.Builder()
                .setText(":)")
                .setBackgroundColor(0xffffcd33)
                .setTextColor(0xff3E4757)
                .build();
        bottomNavigation.setNotification(notification, 1);
```

# API:

## class AHBottomNavigation
**public void addItemAtIndex(int index, AHBottomNavigationItem item)**
- description:add item by index

**public void addItem(AHBottomNavigationItem item)**
- description:add item

**public void addItems(List<AHBottomNavigationItem> items)**
- description:add items

**public void removeItemAtIndex(int index)**
- description:remove item

**public void removeAllItems()**
- description:revmoe all items

**public void refresh()**
- description:refresh

**public void enableItemAtPosition(int position)**
- description:enable item with position

**public void disableItemAtPosition(int position)**
- description:disable item with position

**public void setItemDisableColor(int itemDisableColor)**
- description:set item disable color

**public int getItemsCount()**
- description:get items count

**public boolean isColored()**
- description:is colored mode

**public int getDefaultBackgroundColor()**
- description:get default background color

**public void setDefaultBackgroundColor(int defaultBackgroundColor)**
- description:set default background color

**public int getAccentColor()**
- description:get accent color

**public void setAccentColor(int accentColor)**
- description:set accent color

**public int getInactiveColor()**
- description:get incactive color

**public void setInactiveColor(int inactiveColor)**
- description:set inactive color

**public void setColoredModeColors(int colorActive, int colorInactive)**
- description:set colored mode active color and inactive color

**public void setSelectedBackgroundVisible(boolean visible)**
- description:set selected background visible

**public void setTitleTextSize(int activeSize, int inactiveSize)**
- description:set tile text size

**public AHBottomNavigationItem getItem(int position)**
- description:get item

**public int getCurrentItem()**
- description:get current item

**public void setCurrentItem(int position)**
- description:set current item

**public void setCurrentItem(int position, boolean useCallback)**
- description:set current item with callback

**public void hideBottomNavigation()**
- description:hide bottom navigation

**public void hideBottomNavigation(boolean withAnimation)**
- description:hide bottom navigation with animation

**public void restoreBottomNavigation()**
- description:show bottom navigation

**public void restoreBottomNavigation(boolean withAnimation)**
- description:show bottom navigation with animation

**public boolean isForceTint()**
- description:is force tint

**public void setForceTint(boolean forceTint)**
- description:set force tint

**public TitleState getTitleState()**
- description:get title state

**public void setTitleState(TitleState titleState)**
- description:set title state

**public void setOnTabSelectedListener(OnTabSelectedListener tabSelectedListener)**
- description:set tab selected listener

**public void removeOnTabSelectedListener()**
- description:remove tab selected listener

**public void setOnNavigationPositionListener(OnNavigationPositionListener navigationPositionListener)**
- description:set navigation position Y listener

**public void removeOnNavigationPositionListener()**
- description:remove naviagetion position Y listener

**public void setNotification(int nbNotification, int itemPosition)**
- description:set notification by count string and position

**public void setNotification(String title, int itemPosition)**
- description:set notification by title string and position

**public void setNotification(AHNotification notification, int itemPosition)**
- description:set nitification by item position

**public void setNotificationTextColor(int textColor)**
- description:set notification default text color

**public void setNotificationBackgroundColor(int color)**
- description:set notification default background color

**public void setNotificationMargin(int marginLeft, int marginTop)**
- description:set notification left margin and top margin

**public void setUseElevation (int elevation)**
- description:use elevation (default 8dp)

**public void setUseElevation(boolean useElevation, int elevation)**
- description:use elevatioin with elevation height

**public boolean isHidden()**
- description:is navigation hidden

**public void setSelectHideNotification(boolean selectHideNotification)**
- description:set notification hide if selected

**public boolean getSelectHideNotification()**
- description:get notification hide if selected

**public int getTitleColorActive()**
- description:get title active color on default mode

**public int getTitleColorInactive()**
- description:get title incative color on default mode

**public int getColoredTitleColorActive()**
- description:get title active color on colored mode

**public int getColoredTitleColorInactive()**
- description:get title inactive color on colored mode

**public int getItemDisableColor()**
- description:get title disable color

**public void setTouchEffect(boolean touchEffect)**
- description:set touch effect

**public boolean getTouchEffect()**
- description:get touch effect

## class AHBottomNavigationItem
**public String getTitle()
- description:get item title

**public void setTitle(String title)
- description:set item title

**public int getColor()
- description:get item color

**public void setColor(int color)
- description:set item color

## class AHNotification
**public boolean isEmpty()**
- description:is notification empty

**public String getText()**
- description:get notification text string

**public int getTextColor()**
- description:get notification text color

**public int getBackgroundColor()**
- description:get notification background color

**public static AHNotification justText(String text)**
- description:create notification by title

**public static List<AHNotification> generateEmptyList(int size)**
- description:create notifications by size

# AttrSet
|name|format|description|
|:---:|:---:|:---:|
| selectedBackgroundVisible | boolean | set background visible if selected
| accentColor | color | set active color
| inactiveColor | color | set inactive color
| disableColor | color | set disable color
| coloredActive | color | set active color on colored mode
| coloredInactive | color | set inactive color on colored mode
| colored | boolean | set colored mode
| elevation | dimension | set elevation height


## License
```
AHBottomNavigation library for Android
Copyright (c) 2018 Aurelien Hubert (http://github.com/aurelhubert).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```