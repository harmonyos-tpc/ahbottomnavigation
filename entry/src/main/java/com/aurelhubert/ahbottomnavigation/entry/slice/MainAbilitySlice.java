package com.aurelhubert.ahbottomnavigation.entry.slice;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHNotification;

import com.aurelhubert.ahbottomnavigation.entry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.ToggleButton;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;

import java.util.ArrayList;

public class MainAbilitySlice extends AbilitySlice {
    AHBottomNavigation bottomNavigation;
    ToggleButton colored;
    ToggleButton fiveItems;
    ToggleButton show;
    ToggleButton background;
    Text state;
    Text select;

    String[] titleStateItems =
            new String[] {"SHOW_WHEN_ACITVE", "SHOW_WHEN_ACTIVE_FORCE", "ALWAYS_SHOW", "ALWAYS_HIDE"};

    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        bottomNavigation = (AHBottomNavigation) findComponentById(ResourceTable.Id_bn);
        colored = (ToggleButton) findComponentById(ResourceTable.Id_colored);
        fiveItems = (ToggleButton) findComponentById(ResourceTable.Id_items);
        show = (ToggleButton) findComponentById(ResourceTable.Id_show);
        background = (ToggleButton) findComponentById(ResourceTable.Id_background);

        show.setChecked(true);
        state = (Text) findComponentById(ResourceTable.Id_state);
        state.setText(titleStateItems[0]);
        select = (Text) findComponentById(ResourceTable.Id_select);

        colored.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton absButton, boolean b) {
                        refresh();
                    }
                });
        fiveItems.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton absButton, boolean b) {
                        refresh();
                    }
                });
        show.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton absButton, boolean b) {
                        if (b) {
                            bottomNavigation.restoreBottomNavigation();
                        } else {
                            bottomNavigation.hideBottomNavigation();
                        }
                    }
                });
        background.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton absButton, boolean b) {
                        refresh();
                    }
                });

        state.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        ListDialog listDialog = new ListDialog(getContext());
                        listDialog.setItems(titleStateItems);
                        listDialog.setOnSingleSelectListener(
                                new IDialog.ClickedListener() {
                                    @Override
                                    public void onClick(IDialog iDialog, int i) {
                                        state.setText(titleStateItems[i]);
                                        if (AHBottomNavigation.TitleState.values()[i]
                                                != bottomNavigation.getTitleState()) {
                                            bottomNavigation.setTitleState(AHBottomNavigation.TitleState.values()[i]);
                                            refresh();
                                        }
                                        listDialog.hide();
                                    }
                                });
                        listDialog.show();
                    }
                });

        refresh();
    }

    public void refresh() {
        bottomNavigation.removeAllItems();
        bottomNavigation.setColored(colored.isChecked());
        bottomNavigation.setSelectedBackgroundVisible(background.isChecked());
        bottomNavigationItems.clear();
        if (!fiveItems.isChecked()) {
            AHBottomNavigationItem item1 =
                    new AHBottomNavigationItem(
                            "Menu 1", ResourceTable.Media_ic_home_white_24dp, 0xff455C65, getContext());
            AHBottomNavigationItem item2 =
                    new AHBottomNavigationItem(
                            "Menu 2", ResourceTable.Media_ic_maps_local_bar, 0xff00886A, getContext());
            AHBottomNavigationItem item3 =
                    new AHBottomNavigationItem(
                            "Menu 3", ResourceTable.Media_ic_maps_local_restaurant, 0xff8B6B62, getContext());

            bottomNavigationItems.add(item1);
            bottomNavigationItems.add(item2);
            bottomNavigationItems.add(item3);
            bottomNavigation.setSelectHideNotification(false);
            bottomNavigation.addItems(bottomNavigationItems);
        } else {
            AHBottomNavigationItem item1 =
                    new AHBottomNavigationItem(
                            "Menu 1", ResourceTable.Media_ic_home_white_24dp, 0xff455C65, getContext());
            AHBottomNavigationItem item2 =
                    new AHBottomNavigationItem(
                            "Menu 2", ResourceTable.Media_ic_maps_local_bar, 0xff00886A, getContext());
            AHBottomNavigationItem item3 =
                    new AHBottomNavigationItem(
                            "Menu 3", ResourceTable.Media_ic_maps_local_restaurant, 0xff8B6B62, getContext());

            bottomNavigationItems.add(item1);
            bottomNavigationItems.add(item2);
            bottomNavigationItems.add(item3);

            AHBottomNavigationItem item4 =
                    new AHBottomNavigationItem(
                            "Menu 4", ResourceTable.Media_ic_maps_local_attraction, 0xff6C4A42, getContext());
            AHBottomNavigationItem item5 =
                    new AHBottomNavigationItem("Menu 5", ResourceTable.Media_ic_maps_place, 0xffF63D2B, getContext());

            bottomNavigation.addItem(item4);
            bottomNavigation.addItem(item5);
            bottomNavigation.addItems(bottomNavigationItems);
            bottomNavigation.setNotification("1", 3);
            bottomNavigation.setSelectHideNotification(true);
        }

        AHNotification notification =
                new AHNotification.Builder()
                        .setText(":)")
                        .setBackgroundColor(0xffffcd33)
                        .setTextColor(0xff3E4757)
                        .build();
        bottomNavigation.setNotification(notification, 1);

        setListener();
    }

    private void setListener(){
        bottomNavigation.setOnTabSelectedListener(
                new AHBottomNavigation.OnTabSelectedListener() {
                    @Override
                    public boolean onTabSelected(int position, boolean wasSelected) {
                        select.setText("Current select: " + position);
                        return false;
                    }
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
